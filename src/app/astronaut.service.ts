import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

import { Astronaut } from './astronaut';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class AstronautService {

  private astronautsUrl = 'api/astronauts';

  constructor(private http: HttpClient) { }

  getAstronauts(): Observable<Astronaut[]> {
    return this.http.get<Astronaut[]>(this.astronautsUrl)
      .pipe(
        catchError(this.handleError('getAstronauts', []))
      );
  }

  getAstornautNo404<Data>(id: number): Observable<Astronaut> {
    const url = `${this.astronautsUrl}/?id=${id}`;
    return this.http.get<Astronaut[]>(url)
      .pipe(
        map(astronauts => astronauts[0]),
        tap(h => {
          const outcome = h ? `fetched` : `did not find`;
        }),
        catchError(this.handleError<Astronaut>(`getAstronaut id=${id}`))
      );
  }

  getAstronaut(id: number): Observable<Astronaut> {
    const url = `${this.astronautsUrl}/${id}`;
    return this.http.get<Astronaut>(url).pipe(
      catchError(this.handleError<Astronaut>(`getAstronaut id=${id}`))
    );
  }

  addAstronaut (astronaut: Astronaut): Observable<Astronaut> {
    return this.http.post<Astronaut>(this.astronautsUrl, astronaut, httpOptions).pipe(
      catchError(this.handleError<Astronaut>('addAstronaut'))
    );
  }

  deleteAstronaut (astronaut: Astronaut | number): Observable<Astronaut> {
    const id = typeof astronaut === 'number' ? astronaut : astronaut.id;
    const url = `${this.astronautsUrl}/${id}`;

    return this.http.delete<Astronaut>(url, httpOptions).pipe(
      catchError(this.handleError<Astronaut>('deleteAstronaut'))
    );
  }

  updateAstronaut (astronaut: Astronaut): Observable<any> {
    return this.http.put(this.astronautsUrl, astronaut, httpOptions).pipe(
      catchError(this.handleError<any>('updateAstronaut'))
    );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }
}

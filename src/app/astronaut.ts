export class Astronaut {
  id: number;
  firstName: string;
  lastName: string;
  birthDate: Date;
  superSkill: string;
}

import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AstronautsComponent }      from './astronauts/astronauts.component';
import { AstronautDetailComponent }  from './astronaut-detail/astronaut-detail.component';

const routes: Routes = [
  { path: '', redirectTo: '/astronauts', pathMatch: 'full' },
  { path: 'detail/:id', component: AstronautDetailComponent },
  { path: 'astronauts', component: AstronautsComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}

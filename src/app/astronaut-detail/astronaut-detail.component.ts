import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Astronaut }         from '../astronaut';
import { AstronautService }  from '../astronaut.service';

@Component({
  selector: 'app-astronaut-detail',
  templateUrl: './astronaut-detail.component.html',
  styleUrls: [ './astronaut-detail.component.css' ]
})
export class AstronautDetailComponent implements OnInit {
  @Input() astronaut: Astronaut;

  constructor(
    private route: ActivatedRoute,
    private astronautService: AstronautService,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.getAstronaut();
  }

  getAstronaut(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.astronautService.getAstronaut(id)
      .subscribe(astronaut => {
        this.astronaut = astronaut
      });
  }

  goBack(): void {
    this.location.back();
  }

 save(): void {
    this.astronautService.updateAstronaut(this.astronaut)
      .subscribe(() => this.goBack());
  }
}

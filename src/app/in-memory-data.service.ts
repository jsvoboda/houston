import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const astronauts = [
      { id: 1, firstName: 'Neil', lastName: 'Armstrong', birthDate: new Date(1930, 7, 5), superSkill: 'Big leap'},
      { id: 2, firstName: 'Buzz ', lastName: 'Aldrin', birthDate: new Date(1930, 0, 20), superSkill: 'The second guy'},
      { id: 3, firstName: 'Chris  ', lastName: 'Hadfield', birthDate: new Date(1959, 7, 29), superSkill: 'Showman'},
    ];
    return {astronauts};
  }
}

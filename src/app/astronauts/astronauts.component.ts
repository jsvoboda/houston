import { Component, OnInit } from '@angular/core';

import { Astronaut } from '../astronaut';
import { AstronautService } from '../astronaut.service';

@Component({
  selector: 'app-astronauts',
  templateUrl: './astronauts.component.html',
  styleUrls: ['./astronauts.component.css']
})
export class AstronautsComponent implements OnInit {
  astronauts: Astronaut[];

  constructor(private astronautService: AstronautService) { }

  ngOnInit() {
    this.getAstronauts();
  }

  getAstronauts(): void {
    this.astronautService.getAstronauts()
    .subscribe(astronauts => this.astronauts = astronauts);
  }

  add(astronaut: Astronaut): void {
    astronaut.id = findFreeId(this.astronauts);
    astronaut.birthDate = new Date(astronaut.birthDate);

    this.astronautService.addAstronaut(astronaut)
      .subscribe(astronaut => {
        this.astronauts.push(astronaut);
      });

    function findFreeId(astronauts) {
      let id = 1;
      while (astronauts.some(astronaut => astronaut.id === id)) {
        id++;
      } 
      return id;
    }
  }

  delete(astronaut: Astronaut): void {
    this.astronauts = this.astronauts.filter(h => h !== astronaut);
    this.astronautService.deleteAstronaut(astronaut).subscribe();
  }
}
